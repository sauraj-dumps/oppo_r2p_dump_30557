## sdm660_64-user 10 QKQ1.191014.001 eng.root.20201016.161857 release-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: r2p
- Brand: OPPO
- Flavor: aosp_r2p-userdebug
- Release Version: 11
- Id: RQ3A.211001.001
- Incremental: 1638314901
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OPPO/RMX1801/RMX1801:10/QKQ1.191014.001/1602573502:user/release-keys
- OTA version: 
- Branch: sdm660_64-user-10-QKQ1.191014.001-eng.root.20201016.161857-release-keys
- Repo: oppo_r2p_dump_30557


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
